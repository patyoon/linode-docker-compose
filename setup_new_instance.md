# Setting up a new instance

```
apt-get update && apt-get upgrade
adduser patrick_yoon
adduser patrick_yoon sudo
```

## setup sudo `patrick_yoon` account.

```
sudo groupadd admin
sudo usermod patrick_yoon -g admin
```

Run `sudo visudo` and add

```
%admin  ALL=(ALL) NOPASSWD:ALL
```

## Security

- disable `root` ssh access.

```
sudo nano /etc/ssh/sshd_config
```

Set `PermitRootLogin no`

- ![setup fail2ban]( https://www.linode.com/docs/security/using-fail2ban-for-security/)

```
sudo apt-get install fail2ban
sudo ufw allow ssh
sudo ufw enable
```

## Pull this repo

- setup gitlab ssh-keys

## ![setup docker and docker-compose]( https://docs.docker.com/v17.09/engine/installation/linux/docker-ce/ubuntu/#install-docker-ce-1)

```
sudo apt-get install     apt-transport-https     ca-certificates     curl     software-properties-common
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
sudo apt-key fingerprint 0EBFCD88
sudo add-apt-repository    "deb [arch=amd64] https://download.docker.com/linux/ubuntu \     $(lsb_release -cs) stable"
sudo apt-get install docker-ce
```
