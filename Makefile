restart_prod:
	docker-compose pull
	docker-compose down -v
	docker-compose -f docker-compose.yml up --force-recreate -d
restart_dev:
	docker-compose -f docker-compose-dev.yml up
