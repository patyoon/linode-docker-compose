# Docker compose for patyoon.com

Runs 3 containers - the website, nginx, and letsencrypt (runs cert renewal)

## Install latest `docker-compose`

Need for `-V` option.

https://docs.docker.com/compose/install/#upgrading

## Setting up a new instance

Need to run `docker-compose-dev.yml` to first isntall cert on the machine with a dev image that runs the site on http.

```
make restart_dev
```

## Restarting prod containers

```
make restart_prod
```
